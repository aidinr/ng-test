var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
    $http.get('main.json')
    .then(function(response, status) {
        $scope.status = status;
        $scope.myData = response.data.records;

    }).error(function(response, status) {
           $scope.messages = response || "Request failed";
           $scope.status = status;
       });;

});

